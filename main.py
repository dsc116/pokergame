import random
from collections import defaultdict
#Poker game
#Idea: have a pack of card then dole out two to every player #player is always player 0
def deal(numofplayers):
    deck = list(range(52)) #list from 1 to 52 then assign suits to be each set of 13 (will make 2 lowest)
    players = []
    for i in range(numofplayers):
        hand = random.sample(deck,2)
        players.append(hand) #Second entry is the winning criteria
        deck.remove(hand[0])
        deck.remove(hand[1])
        for i in range(2):
            h = hand[i]
            val = h%13 + 2 #0 is 2
            suit = h//13
            hand[i] = (val,suit) #Last entry makes it easy for computers to do calculations
        shared = random.sample(deck,5)
        for i in range(5):
            h = shared[i]
            val = h%13 + 2 #0 is 2
            suit = h//13
            shared[i] = (val,suit) #Last entry makes it easy for computers to do calculations
        #Game dealt and converted
    return players, shared
    #Winning conditions - players have two values, first is type of hand, second is kicker value
#    for playeri in players:
#        hand = playeri[0] + shared
#        #hand = sorted(hand, key=lambda x: x[1])
#        vals = [i[0] for i in hand]
#        val_count = defaultdict(lambda:0)
#        for v in vals:
#            val_count[v] += 1
#        suits = [i[1] for i in hand]
#
#        print(hand)
#        print(vals)
#        print(suits)
#    return players

def check_four_of_a_kind(hand): #If drawn then winner has highest kicker
    vals = [i[0] for i in hand]
    val_count = defaultdict(lambda:0)
    for v in vals:
        val_count[v] += 1
    if sorted(val_count.values())[-1] == 4:
        print(val_count)
        print(val_count[4])
        return True
    else:
        return False

def check_fullhouse(hand): #Will add what value card - winner has highest three then pair
    vals = [i[0] for i in hand]
    val_count = defaultdict(lambda:0)
    for v in vals:
        val_count[v] += 1
    if (sorted(val_count.values())[-2:0] == [2,3]) or (sorted(val_count.values())[-2:0] == [3,3]):
        return True
    else:
        return False

def check_flush(hand): #Will return highest card and list of flush hand - highest card in flush wins(all the way to fifth card)
    suits = [i[0] for i in hand]
    suit_count = defaultdict(lambda:0)
    for s in suits:
        suit_count[s] += 1
    checkval = sorted(suit_count.values())[-1]
    if (checkval >= 5):
        newhand = [i for i in hand if i[1]==suit_count[checkval]]
        return True, newhand
    else:
        return False, 0

def check_straight(hand): #Will return highest card and list of flush hand
    high = 0
    vals = [i[0] for i in hand]
    setval = list(set(vals))
    print(setval)
    length = len(setval)
    if length>=5:
        for i in range(length-4):
            if (setval[i]+4)==setval[i+4]:
                high = setval[i+4]
    if high != 0:
        return True, high
    return False, 0

def check_three_of_a_kind(hand): #If drawn then winner has highest kicker
    vals = [i[0] for i in hand]
    val_count = defaultdict(lambda:0)
    for v in vals:
        val_count[v] += 1
    if sorted(val_count.values())[-1] == 3:
        return True
    else:
        return False
    
def check_two_pair(hand): #If drawn then winner has highest kicker
    vals = [i[0] for i in hand]
    val_count = defaultdict(lambda:0)
    for v in vals:
        val_count[v] += 1
    if sorted(val_count.values())[-2:0] == [2,2]:
        return True
    else:
        return False
    
def check_pair(hand): #If drawn then winner has highest kicker
    vals = [i[0] for i in hand]
    val_count = defaultdict(lambda:0)
    for v in vals:
        val_count[v] += 1
    if sorted(val_count.values())[-1] == 2:
        return True
    else:
        return False

def game(players,shared): #return winner
    winner = [-1,[0,0]]
    for i in range(len(players)):
        hand = players[i] + shared
        score = [0,0]
        if check_four_of_a_kind(hand):
            score = [7,0]
            
        elif check_fullhouse(hand):
            score = [6,0]

        elif check_flush(hand)[0]:
            newhand = check_flush(hand)[1]
            if check_straight(newhand)[0]:
                score = [8,check_straight(newhand)[1]]
            else:
                score = [5,sorted(newhand)[-1]]
        
        elif check_straight(hand)[0]:
            score = [4, check_straight(hand)[1]]
            
        elif check_three_of_a_kind(hand):
            score = [3,0]
        
        elif check_two_pair(hand):
            score = [2,0]
        
        elif check_pair(hand):
            score = [1,0]
        
        else: #Check for highest card
            vals = [i[0] for i in hand]
            high = sorted(vals)[-1]
            score = [0,high]
        #Check who won
        if score[0]> winner[1][0]:
            winner = [i, score]
        elif score[0]==winner[1][0]:
            if score[1]>winner[1][1]:
                winner = [i, score]
            elif score[1]==winner[1][1]:
                drawers = [winner[0]]
                drawers.append(i)
                winner = [drawers, score]
        
            
print(check_four_of_a_kind([(3,0),(3,1),(3,2),(3,3)]))

print(deal(3))

def convert_hand(singlecard): #Converts number card to readable
    val, suit = singlecard
    #Value first
    if val == 11:
        card1 = "J"
    elif val == 12:
        card1 = "Q"
    elif val == 13:
        card1 = "K"
    elif val == 14:
        card1 = "A"
    else:
        card1 = str(val)
    #Suit now (Hearts,Diamonds,Clubs,Spades)
    if suit == 0:
        card2 = "Hearts"
    elif suit == 1:
        card2 = "Diamonds"
    elif suit == 2:
        card2 = "Clubs"
    else:
        card2 = "Spades"
    return (card1,card2)